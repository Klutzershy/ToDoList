/**
 * 
 */
package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * @author Klutzershy
 *
 */
public class ToDoList extends Application {
	
	private MainWindow mainWindow = new MainWindow();
	
	public Scene mainScene = null;
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		Scene scene = new Scene(mainWindow);
		buildKeyPress(scene);
		primaryStage.getIcons().add(new Image(ToDoList.class.getResourceAsStream("notepad.png")));
		primaryStage.setMinWidth(250.0);
		primaryStage.setWidth(250.0);
		primaryStage.setHeight(400.0);
		primaryStage.setMaxHeight(450.0);
		primaryStage.setMaxWidth(350.0);
		primaryStage.setTitle("Task List");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);

	}
	
	@Override
	public void init() {
		load();
	}
	
	@Override
	public void stop() {
		
		save();
		
	}
	
	private void save() {
//		Path file = Paths.get("task_list_items.txt");
//		Files.write(file, mainWindow.getLi);
		
		try (FileWriter writer = new FileWriter("task_list_items.txt"); BufferedWriter bw = new BufferedWriter(writer)){
			
			for(Note note : mainWindow.getListView().getItems()) {
				bw.write("{");
				bw.newLine();
				bw.write(note.getText());
				bw.write(",");
				if(note.getDueDate() != null) {
					bw.write(note.getDueDate().toString());
				} else {
					bw.write("none");
				}
				bw.newLine();
				bw.write("}");
				bw.newLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private void load() {
		try (FileReader reader = new FileReader("task_list_items.txt"); BufferedReader br = new BufferedReader(reader)) {
			String line = null;
			while((line = br.readLine()) != null) {
				if(line.equals("{")) {
					Note newNote = null;
					String noteline = br.readLine();
					String[]noteSplit = noteline.split(",");
					if(noteSplit[1].equals("none")) {
						newNote = new Note(noteSplit[0]);
					} else {
						String[]dateSplit = noteSplit[1].split("-");
						int year = Integer.parseInt(dateSplit[0]);
						int month = Integer.parseInt(dateSplit[1]);
						int day = Integer.parseInt(dateSplit[2]);
						newNote = new Note(noteSplit[0],LocalDate.of(year,month,day));
					}
					if(newNote != null) {
						mainWindow.getListView().getItems().add(newNote);
					}	
				}
			}
			
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void buildKeyPress(Scene scene) {
		scene.addEventFilter(KeyEvent.KEY_RELEASED, event -> {
			final KeyCombination keyComb = new KeyCodeCombination(KeyCode.ENTER);
			
			if(keyComb.match(event)) {
				mainWindow.addNote();
			}
		});
		
		scene.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
			final KeyCombination keyComb = new KeyCodeCombination(KeyCode.Z,KeyCombination.CONTROL_DOWN);
			if(keyComb.match(event)) {
				mainWindow.restore();
			}
		});
		
		scene.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
			final KeyCombination keyComb = new KeyCodeCombination(KeyCode.S,KeyCombination.CONTROL_DOWN);
			if(keyComb.match(event)) {
				save();
			}
		});
	}

}
