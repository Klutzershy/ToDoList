/**
 * 
 */
package main;

import java.time.LocalDate;

import javafx.beans.property.SimpleStringProperty;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 * @author Klutzershy
 *
 */
public class Note extends VBox {

	
	private SimpleStringProperty text = new SimpleStringProperty("");
	
	private LocalDate dueDate = null;
	
	private Long longDate = new Long(Long.MAX_VALUE);
	
	public Long getLongDate() {
		return longDate;
	}

	public Note(String text) {
		this.text.set(text);
		buildNoteObject();
	}
	
	public Note(String text, LocalDate dueDate) {
		this.text.set(text);
		this.dueDate = dueDate;
		this.longDate = dueDate.toEpochDay();
		buildNoteObject();
	}
	
	private void buildNoteObject() {
//		this.setWidth(this.getScene().getWidth());
		this.setHeight(50);
		Rectangle header = new Rectangle();
		header.setFill(Paint.valueOf("#f2e985"));
		header.setHeight(20);
		header.setWidth(220);
//		setHeight(500);
//		setWidth(USE_COMPUTED_SIZE);
		Rectangle rect = new Rectangle();
		rect.setFill(Paint.valueOf("#f8f6a6"));
		rect.setHeight(40);
		rect.setWidth(220);

		StackPane headerStack = new StackPane(); //Task Header with date
		StackPane stack = new StackPane(); // Note info stack
		Text textObj = new Text(text.get());
		headerStack.getChildren().add(header);
		stack.getChildren().addAll(rect, textObj);
		if(dueDate != null) {
			Text dateObj = new Text(dueDate.toString());
			headerStack.getChildren().add(dateObj);
			this.getChildren().addAll(headerStack, stack);
		} else {
			this.getChildren().addAll(headerStack, stack);
		}
	}

	public String getText() {
		return text.get();
	}

	public LocalDate getDueDate() {
		return dueDate;
	}
}
