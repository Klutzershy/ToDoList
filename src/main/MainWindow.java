/**
 * 
 */
package main;

import java.util.ArrayList;
import java.util.Comparator;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 * @author Klutzershy
 *
 */
public class MainWindow extends VBox {
	
	private TextField noteInput = new TextField();
	
	private DatePicker datePicker = new DatePicker();
	
	private ObservableList<Note> obList = FXCollections.observableArrayList();
	private ListView<Note> noteList = new ListView<>(obList);
	
	private ArrayList<Note> backupList = new ArrayList<Note>();
	
	private Comparator<Note> comparator = null;
	
	
	public MainWindow() {
		StackPane helpPane = new StackPane();
		Rectangle helpRect = new Rectangle();
		helpRect.setWidth(this.getWidth());
		helpRect.setFill(Paint.valueOf("#bfa870"));
		Text helpText = new Text("2x Click = Delete      Ctrl + Z = Undo");
		helpPane.getChildren().addAll(helpRect,helpText);
		noteInput.setPromptText("Note Text");
		datePicker.setPrefWidth(110.0);
		datePicker.setMinWidth(110.0);
		noteInput.setPrefWidth(Integer.MAX_VALUE);
		noteList.setStyle("-fx-selection-bar: #d0cf8d");
		
		HBox hBox = new HBox();
		HBox.setHgrow(hBox, Priority.ALWAYS);
		hBox.getChildren().addAll(noteInput,datePicker);
		this.getChildren().addAll(helpPane,hBox,noteList);
		
		noteList.setOnMouseClicked(event -> {
			if(event.getClickCount() == 2) {
				removeNote();
			}
		});
		noteList.setStyle("-fx-background-color: #bfa870");
		noteList.setCellFactory(lv -> new ListCell<Note>() {
			{
				setPadding(new Insets(5,0,0,0));
				setStyle("-fx-background-color: #bfa870");
			}
			@Override
			protected void updateItem(Note item, boolean empty) {
				super.updateItem(item, empty);
				if(empty) {
					setGraphic(null);
				} else {
					setGraphic(item);
				}
			}
		});
		
	}
	
	public void addNote() {
		
		if(!noteInput.getText().equals("")) {
			if(!datePicker.getEditor().getText().equals("")) {
				Note newNote = new Note(noteInput.getText(), datePicker.getValue());
				if(obList.add(newNote)) {
					noteInput.clear();
					datePicker.getEditor().clear();
				}
			} else {
				Note newNote = new Note(noteInput.getText());
				if(obList.add(newNote)) {
					noteInput.clear();
					datePicker.getEditor().clear();
				}
			}
			try {
			comparator = new Comparator<Note>() {

				@Override
				public int compare(Note o1, Note o2) {

					return o1.getLongDate().compareTo(o2.getLongDate());
				}
				
			};
			FXCollections.sort(obList, comparator);
			}catch(NullPointerException e){
				e.printStackTrace();
			};
		}
	}
	
	public void removeNote() {
		Note noteToRemove = noteList.getSelectionModel().getSelectedItem();
		obList.remove(noteToRemove);
		backupList.add(noteToRemove);
	}
	
	public void restore() {
		if(backupList.size() > 0) {
			obList.add(backupList.get(0));
			backupList.remove(0);
			FXCollections.sort(obList, comparator);
		}
	}
	
	public ListView<Note> getListView() {
		return noteList;
	}

}
